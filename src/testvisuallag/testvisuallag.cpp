 
#include "ros/ros.h"
#include <opencv2/core/core.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <std_msgs/Empty.h>

using namespace cv;

cv::Mat cv_image;

void showMarker(std_msgs::Empty msg)
{
    
    cv::namedWindow("Result", WINDOW_AUTOSIZE);
  
    imshow("Result", cv_image);
    waitKey(0);
    
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "testvisuallag");
  
  ros::NodeHandle n;
  
  cv_image=imread("/home/alex/artag.png");

  ros::Subscriber sub       = n.subscribe("/test_video_lag", 1, &showMarker);

  ros::spin();

  return 0;
}
