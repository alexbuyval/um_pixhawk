 
#include "MarkerPoseNode.h"
#include "ros/ros.h"


int main(int argc, char **argv)
{

  ros::init(argc, argv, "markerpose");

  MarkerPoseNode n;

  ros::spin();

  return 0;
}
