

#include "MarkerPoseNode.h"
#include "ros/ros.h"
#include "ros/package.h"
#include "geometry_msgs/Twist.h"
#include <sys/stat.h>
#include <string>

using namespace std;

MarkerPoseNode::MarkerPoseNode()
{
    imudata_channel = nh_.resolveName("/mavros/imu/data");
    ros::param::get("~convert_to_NED", convertToNED);
    ros::param::get("~pose_topic", pose_topic);

    imudata_sub       = nh_.subscribe(imudata_channel, 10, &MarkerPoseNode::imudataCb, this);
    markers_sub       = nh_.subscribe("/ar_pose_marker", 10, &MarkerPoseNode::markersCb, this);
    pose_pub =  nh_.advertise<geometry_msgs::PoseStamped>(pose_topic,1);
}

MarkerPoseNode::~MarkerPoseNode()
{

}

void MarkerPoseNode::imudataCb(const sensor_msgs::ImuConstPtr imudataPtr)
{
     lastImudataReceived = *imudataPtr;
}

void MarkerPoseNode::markersCb(const ar_track_alvar_msgs::AlvarMarkersConstPtr markers)
{
    if (markers->markers.size()>0)
    {

        Matrix<float, 3, 3> Tbn;
        Tbn.setZero();

        Eigen::Quaternionf q(lastImudataReceived.orientation.w, lastImudataReceived.orientation.x, lastImudataReceived.orientation.y, lastImudataReceived.orientation.z);
        Tbn = q.toRotationMatrix();

        Eigen::Vector3f marker_pos_body;
        //convert from ar_track coordinates to body coordinates
        marker_pos_body(0) = -markers->markers[0].pose.pose.position.y;
        marker_pos_body(1) = -markers->markers[0].pose.pose.position.x;
        marker_pos_body(2) = -markers->markers[0].pose.pose.position.z;

        Eigen::Vector3f marker_pos_NED;
        marker_pos_NED.setZero();

        Eigen::Vector3f pos;
        pos.setZero();
        pos = marker_pos_NED - Tbn*marker_pos_body;


        //publish marker pose
        geometry_msgs::PoseStamped pose_msg;
        if(convertToNED)
        {
            pose_msg.pose.position.x = pos(0);
            pose_msg.pose.position.y = pos(1);
            pose_msg.pose.position.z = pos(2);
        }else
        {
            pose_msg.pose.position.x = marker_pos_body(0);
            pose_msg.pose.position.y = marker_pos_body(1);
            pose_msg.pose.position.z = marker_pos_body(2);
        }
        pose_msg.header.stamp = ros::Time::now();
        pose_pub.publish(pose_msg);
        ROS_INFO("ARTag Id=%d x=%.2f y=%.2f z=%.2f",
                 markers->markers[0].id,
                pose_msg.pose.position.x, pose_msg.pose.position.y, pose_msg.pose.position.z);
    }
}



