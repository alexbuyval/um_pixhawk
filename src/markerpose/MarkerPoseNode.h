#pragma once

#ifndef __MARKERNODE_H
#define __MARKERNODE_H
 

#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Vector3Stamped.h"
#include "sensor_msgs/Imu.h"
#include "sensor_msgs/Image.h"
#include <dynamic_reconfigure/server.h>
#include <ar_track_alvar_msgs/AlvarMarkers.h>
#include <eigen3/Eigen/Eigen>
using namespace Eigen;



struct MarkerPoseNode
{
private:

    ros::Subscriber markers_sub; //ar_tag alvar data
    ros::Subscriber imudata_sub; // drone navdata
    // output
    ros::Publisher pose_pub;

    ros::NodeHandle nh_;

    std::string imudata_channel;
    std::string pose_topic;

    bool convertToNED;

    // save last imu received for forwarding...
    sensor_msgs::Imu lastImudataReceived;

    // ROS message callbacks
    void imudataCb(const sensor_msgs::ImuConstPtr imudataPtr);
    void markersCb(const ar_track_alvar_msgs::AlvarMarkersConstPtr markers);

public:

    MarkerPoseNode();
    ~MarkerPoseNode();


};
#endif /* __MARKERNODE_H */
