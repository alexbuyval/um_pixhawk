#include "um_pixhawk/PathPlan.h"
#include "um_pixhawk/plan_response.h"
#include "um_pixhawk/plan_request.h"
#include "ros/ros.h"
#include "geometry_msgs/Pose.h"

int main(int argc, char **argv)
{

  ros::init(argc, argv, "path_planner_client");

  ros::NodeHandle n;

  
  ros::ServiceClient client = n.serviceClient<um_pixhawk::PathPlan>("PathPlan");
  um_pixhawk::PathPlan srv;
  geometry_msgs::Pose start_pose;
  start_pose.position.x = 200.0;
  start_pose.position.y = 50.0;
  start_pose.position.z = -200.0;
  geometry_msgs::Pose goal_pose;
  goal_pose.position.x = 200.0;
  goal_pose.position.y = 50.0;
  goal_pose.position.z = -400.0;
  srv.request.planrequest.start_pose = start_pose;
  srv.request.planrequest.goal_pose = goal_pose;
  srv.request.planrequest.env_mesh = "/home/alex/omplapp/resources/3D/Easy_env.dae";
  
  if (client.call(srv))
  {
    ROS_INFO("Succes");
    nav_msgs::Path resPath = srv.response.planresponse.path;
    for (std::size_t i=0; i<sizeof(resPath.poses); i++)
    {
	geometry_msgs::PoseStamped pose = resPath.poses[i];
	ROS_INFO("State X=%.2f Y=%.2f Z=%.2f", pose.pose.position.x, pose.pose.position.y, pose.pose.position.z);
    }
  }
  else
  {
    ROS_ERROR("Failed to call service PathPlan");
    return 1;
  }


  return 0;
}