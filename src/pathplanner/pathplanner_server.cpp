 
#include "um_pixhawk/PathPlan.h"
#include "um_pixhawk/plan_response.h"
#include "um_pixhawk/plan_request.h"
#include "ros/ros.h"
#include "geometry_msgs/Pose.h"

#include <ompl/tools/benchmark/Benchmark.h>
#include <ompl/control/planners/rrt/RRT.h>
#include <ompl/control/planners/kpiece/KPIECE1.h>
#include <omplapp/apps/QuadrotorPlanning.h>
#include <omplapp/apps/SE3RigidBodyPlanning.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <omplapp/config.h>

using namespace ompl;

void quadrotorSetup(geometry_msgs::Pose start_pose, geometry_msgs::Pose goal_pose, std::string env_fname,  app::QuadrotorPlanning& setup)
{
     base::StateSpacePtr stateSpace(setup.getStateSpace());
 
     // set the bounds for the R^3 part of SE(3)
     base::RealVectorBounds bounds(3);
     bounds.setLow(0);
     bounds.setHigh(400);
     stateSpace->as<base::CompoundStateSpace>()->as<base::SE3StateSpace>(0)->setBounds(bounds);
     
     setup.setEnvironmentMesh(env_fname.c_str());
 
     // define start state
     base::ScopedState<base::SE3StateSpace> start(setup.getGeometricComponentStateSpace());
     start->setX(start_pose.position.x);
     start->setY(start_pose.position.y);
     start->setZ(start_pose.position.z);
     start->rotation().setIdentity();
 
     // define goal state
     base::ScopedState<base::SE3StateSpace> goal(setup.getGeometricComponentStateSpace());
     goal->setX(goal_pose.position.x);
     goal->setY(goal_pose.position.y);
     goal->setZ(goal_pose.position.z);
     goal->rotation().setIdentity();

     // set the start & goal states
     setup.setStartAndGoalStates(
         setup.getFullStateFromGeometricComponent(start),
         setup.getFullStateFromGeometricComponent(goal), .5);
}

void quadrotorDemo(app::QuadrotorPlanning& setup)
{
     std::vector<double> coords;
 
     std::cout<<"\n\n***** Planning for a " << setup.getName() << " *****\n" << std::endl;
     setup.setPlanner(base::PlannerPtr(new control::RRT(setup.getSpaceInformation())));
 
     // try to solve the problem
     if (setup.solve(40))
     {
         // print the (approximate) solution path: print states along the path
         // and controls required to get from one state to the next
         control::PathControl& path(setup.getSolutionPath());
         //path.interpolate(); // uncomment if you want to plot the path
         path.printAsMatrix(std::cout);
 
         if (!setup.haveExactSolutionPath())
         {
             std::cout << "Solution is approximate. Distance to actual goal is " <<
                 setup.getProblemDefinition()->getSolutionDifference() << std::endl;
         }
     }
}

geometric::PathGeometric& SE3RigidDemo(geometry_msgs::Pose start_pose, geometry_msgs::Pose goal_pose, std::string env_fname)
{
     app::SE3RigidBodyPlanning setup;
     std::string robot_fname = "/home/alex/omplapp/resources/3D/quadrotor.dae";
     setup.setRobotMesh(robot_fname.c_str());
     setup.setEnvironmentMesh(env_fname.c_str());
 
     // define start state
     base::ScopedState<base::SE3StateSpace> start(setup.getSpaceInformation());
     start->setX(start_pose.position.x);
     start->setY(start_pose.position.y);
     start->setZ(start_pose.position.z);
     start->rotation().setIdentity();
 
     // define goal state
     base::ScopedState<base::SE3StateSpace> goal(start);
     goal->setX(goal_pose.position.x);
     goal->setY(goal_pose.position.y);
     goal->setZ(goal_pose.position.z);
     goal->rotation().setIdentity();
 
     // set the start & goal states
     setup.setStartAndGoalStates(start, goal);
 
     // setting collision checking resolution to 1% of the space extent
     setup.getSpaceInformation()->setStateValidityCheckingResolution(0.01);

     // use RRTConnect for planning
     setup.setPlanner (base::PlannerPtr(new geometric::RRTConnect(setup.getSpaceInformation())));
     
     // we call setup just so print() can show more information
     setup.setup();
     setup.print();
 
     // try to solve the problem
     if (setup.solve(10))
     {
         // simplify & print the solution
	 setup.simplifySolution();
         setup.getSolutionPath().printAsMatrix(std::cout);
	 return setup.getSolutionPath();
     }
  
}

void SE3StateSpaceToPoseMsg(const ompl::base::SE3StateSpace::StateType &pose,
                          geometry_msgs::PoseStamped &pose_msg)
{
  pose_msg.pose.position.x = pose.getX();
  pose_msg.pose.position.y = pose.getY();
  pose_msg.pose.position.z = pose.getZ();
};

bool plan(um_pixhawk::PathPlan::Request  &req, um_pixhawk::PathPlan::Response &res)
{
  //app::QuadrotorPlanning quadrotor;
  //quadrotorSetup(req.planrequest.start_pose, req.planrequest.goal_pose, req.planrequest.env_mesh, quadrotor);
  //quadrotorDemo(quadrotor);
  ROS_INFO("Start X=%.2f Y=%.2f Z=%.2f", req.planrequest.start_pose.position.x, req.planrequest.start_pose.position.y, req.planrequest.start_pose.position.z);
  ROS_INFO("Goal X=%.2f Y=%.2f Z=%.2f", req.planrequest.goal_pose.position.x, req.planrequest.goal_pose.position.y, req.planrequest.goal_pose.position.z);
  geometric::PathGeometric& path(SE3RigidDemo(req.planrequest.start_pose, req.planrequest.goal_pose, req.planrequest.env_mesh));
  
  nav_msgs::Path resPath; 
  
  for(std::size_t i=0; i<path.getStateCount(); i++)
  {
    ROS_INFO("1");
    base::State *curState=path.getState(i);
    geometry_msgs::PoseStamped pose;
    ROS_INFO("2");
    SE3StateSpaceToPoseMsg(*(path.getState(i)->as<ompl::base::CompoundState>()->as<ompl::base::SE3StateSpace::StateType>(1)),pose);

/*    
    pose.pose.position.x = curState->as<base::CompoundState>()->as<base::RealVectorStateSpace::StateType>()->as<base::RealVectorStateSpace::StateType>()->value[0]; 
    pose.pose.position.y = curState->as<ompl::base::CompoundState>()->as<base::RealVectorStateSpace::StateType>(0)->as<ompl::base::RealVectorStateSpace::StateType>()->value[1];
    pose.pose.position.z = curState->as<ompl::base::CompoundState>()->as<base::RealVectorStateSpace::StateType>(0)->as<ompl::base::RealVectorStateSpace::StateType>()->value[2];*/
    ROS_INFO("3");
    resPath.poses.push_back(pose);
  }
  
  res.planresponse.path = resPath;
  
  return true;
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "path_planner_server");

  ros::NodeHandle n;

  ros::ServiceServer service = n.advertiseService("PathPlan", plan);
  ROS_INFO("Ready to plan paths");
  ros::spin();

  return 0;
}
