#pragma once

#ifndef __HOVERMARKERNODE_H
#define __HOVERMARKERNODE_H
 

#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Vector3Stamped.h"
#include "sensor_msgs/Image.h"
#include "sensor_msgs/Imu.h"
#include "nav_msgs/Odometry.h"
#include "tf/tfMessage.h"
#include "tf/transform_listener.h"
#include "tf/transform_broadcaster.h"
#include "tf2_ros/buffer.h"
#include "tf2_ros/transform_listener.h"
#include <tf2_ros/transform_broadcaster.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include "std_msgs/Empty.h"
#include "std_srvs/Empty.h"
#include "std_msgs/String.h"
#include <dynamic_reconfigure/server.h>
#include <ar_track_alvar_msgs/AlvarMarkers.h>
#include <mavros_msgs/OverrideRCIn.h>



struct HoverMarkerNode
{
private:
	// comm with drone
    ros::Subscriber imudata_sub; // drone navdata
    ros::Subscriber markers_sub; //ar_tag alvar data
    ros::Subscriber rcoverride_sub;
    ros::Subscriber odometry_sub;

    ros::Time lastImuStamp;
    ros::Time lastGPVelStamp;

    // output
    ros::Publisher dronecontrol_pub;
    ros::Publisher markerpose_pub;
    ros::Publisher markervel_pub;

    ros::NodeHandle nh_;

    //ros parameters
    std::string cameraLink;
    bool enableRCControl;

    std::string imudata_channel;
    std::string control_channel;

    // save last imu received for forwarding...
    sensor_msgs::Imu lastImudataReceived;
    nav_msgs::Odometry lastOdodataReceived;
    geometry_msgs::Vector3Stamped lastVelReceived;
    geometry_msgs::Vector3Stamped lastMarkerVel;
    geometry_msgs::PoseStamped lastMarkerPose;
    mavros_msgs::OverrideRCIn lastRCoverride;


    tf2_ros::Buffer tfBuffer;
    tf2_ros::TransformListener* tfListener;

    // ROS message callbacks
    void imudataCb(const sensor_msgs::ImuConstPtr imudataPtr);
    void rcOverrideCb(const mavros_msgs::OverrideRCInConstPtr rcPtr);
    void markersCb(const ar_track_alvar_msgs::AlvarMarkersConstPtr markers);
    void odometryCb(const nav_msgs::OdometryConstPtr ododataPtr);

public:

    HoverMarkerNode();
    ~HoverMarkerNode();


    // main pose-estimation loop
    void Loop();

};
#endif /* __HOVERMARKERNODE_H */
