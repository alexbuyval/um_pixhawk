 
#include "HoverMarkerNode.h"
#include "ros/ros.h"


int main(int argc, char **argv)
{

  ros::init(argc, argv, "hover_marker");

  HoverMarkerNode hover;

  hover.Loop();

  return 0;
}
