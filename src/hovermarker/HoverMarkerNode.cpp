

#include "HoverMarkerNode.h"
#include "ros/ros.h"
#include "ros/package.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/Image.h"
#include "sensor_msgs/Imu.h"
//#include "tf/tfMessage.h"
//#include "tf/transform_listener.h"
//#include "tf/transform_broadcaster.h"
#include "std_msgs/Empty.h"
#include "std_srvs/Empty.h"
#include "std_msgs/String.h"
#include "std_msgs/Empty.h"
#include "std_srvs/Empty.h"
#include <sys/stat.h>
#include <string>

using namespace std;

HoverMarkerNode::HoverMarkerNode()
{
    ROS_INFO("Start of HoverMarkerNode()");
    imudata_channel = nh_.resolveName("/mavros/imu/data");
    control_channel = nh_.resolveName("/mavros/rc/override");

    ros::param::get("~camera_link", cameraLink);

    imudata_sub       = nh_.subscribe(imudata_channel, 10, &HoverMarkerNode::imudataCb, this);
    odometry_sub       = nh_.subscribe("/odometry", 10, &HoverMarkerNode::odometryCb, this);
    rcoverride_sub = nh_.subscribe("/mavros/rc/override_joy", 10, &HoverMarkerNode::rcOverrideCb, this);
    markers_sub       = nh_.subscribe("/ar_pose_marker", 10, &HoverMarkerNode::markersCb, this);

    dronecontrol_pub	   = nh_.advertise<mavros_msgs::OverrideRCIn>(control_channel,1);

    markerpose_pub =  nh_.advertise<geometry_msgs::PoseStamped>("/marker_pose",1);
    markervel_pub =  nh_.advertise<geometry_msgs::Vector3Stamped>("/marker_velocity",1);
    //tf_broadcaster();
    tfListener = new tf2_ros::TransformListener(tfBuffer);

}

HoverMarkerNode::~HoverMarkerNode()
{

}

void HoverMarkerNode::imudataCb(const sensor_msgs::ImuConstPtr imudataPtr)
{
    lastImudataReceived = *imudataPtr;    
}

void HoverMarkerNode::odometryCb(const nav_msgs::OdometryConstPtr ododataPtr)
{
    lastOdodataReceived = *ododataPtr;    
}

void HoverMarkerNode::rcOverrideCb(const mavros_msgs::OverrideRCInConstPtr rcPtr)
{
    lastRCoverride = *rcPtr;
}


void HoverMarkerNode::markersCb(const ar_track_alvar_msgs::AlvarMarkersConstPtr markers)
{
    if (markers->markers.size()>0)
    {

        try{

            //publish marker pose
            geometry_msgs::PoseStamped markerpose_msg = markers->markers[0].pose;
            markerpose_msg.header.stamp = ros::Time::now();
            markerpose_pub.publish(markerpose_msg);

            double timeBetweenMarkers = (ros::Time::now() - lastMarkerVel.header.stamp).toSec();
            lastMarkerVel.header.stamp = ros::Time::now();

            if(timeBetweenMarkers < 1.0)
            {
                lastMarkerVel.vector.x = (markerpose_msg.pose.position.x - lastMarkerPose.pose.position.x)/timeBetweenMarkers;
                lastMarkerVel.vector.y = (markerpose_msg.pose.position.y - lastMarkerPose.pose.position.y)/timeBetweenMarkers;
            } else
            {
                lastMarkerVel.vector.x = 0.0;
                lastMarkerVel.vector.y = 0.0;
            }

            markervel_pub.publish(lastMarkerVel);

            ROS_INFO("Observe ARTag Id=%d x=%.2f y=%.2f z=%.2f vx=%.2f vy=%.2f time=%.2f",
                     markers->markers[0].id,
                     markerpose_msg.pose.position.x, markerpose_msg.pose.position.y, markerpose_msg.pose.position.z,
                     lastMarkerVel.vector.x, lastMarkerVel.vector.y, timeBetweenMarkers);

            lastMarkerPose = markerpose_msg;

            static tf2_ros::TransformBroadcaster br;
            geometry_msgs::TransformStamped transformStamped;

            transformStamped.header.stamp = ros::Time::now();
            transformStamped.header.frame_id = cameraLink;
            transformStamped.child_frame_id = "ar_marker_3";
            transformStamped.transform.translation.x = markerpose_msg.pose.position.y;
            transformStamped.transform.translation.y = markerpose_msg.pose.position.x;
            transformStamped.transform.translation.z = -markerpose_msg.pose.position.z;
            double marker_roll_, marker_pitch_, marker_yaw_;
            tf::Quaternion q;
            tf::quaternionMsgToTF(markerpose_msg.pose.orientation, q);
            tf::Matrix3x3(q).getRPY(marker_roll_, marker_pitch_, marker_yaw_);
            double drone_roll_, drone_pitch_, drone_yaw_;
            tf::Quaternion q2;
            tf::quaternionMsgToTF(lastImudataReceived.orientation, q2);
            //tf::quaternionMsgToTF(lastOdodataReceived.pose.pose.orientation, q2);
	    tf::Matrix3x3(q2).getRPY(drone_roll_, drone_pitch_, drone_yaw_);

            q.setRPY(-drone_roll_, -drone_pitch_, marker_yaw_-M_PI);
            transformStamped.transform.rotation.x = q.x();
            transformStamped.transform.rotation.y = q.y();
            transformStamped.transform.rotation.z = q.z();
            transformStamped.transform.rotation.w = q.w();

            br.sendTransform(transformStamped);


        }
        catch (tf2::TransformException &ex) {
            ROS_ERROR("%s",ex.what());
        }
    }
}


void HoverMarkerNode::Loop()
{
  ros::Rate pub_rate(30);
  
  while (nh_.ok())
  {
    ros::spinOnce();
    
    mavros_msgs::OverrideRCIn control_msg = lastRCoverride;
    
    if(ros::Time::now() - lastMarkerPose.header.stamp < ros::Duration(2.0))
    {
      
      if (lastRCoverride.channels[0]==1500 && lastRCoverride.channels[1]==1500) //use auto control
      {
	uint16_t roll_control = 1500 + uint16_t(70*lastMarkerPose.pose.position.x+20*lastMarkerVel.vector.x);
	uint16_t pitch_control = 1500 + uint16_t(70*lastMarkerPose.pose.position.y+20*lastMarkerVel.vector.y);
	
	control_msg.channels[0] = roll_control;
	control_msg.channels[1] = pitch_control;
      }
    }
    
    // publish!
    dronecontrol_pub.publish(control_msg);
    
    
    // --------------  sleep until rate is hit. ---------------
    pub_rate.sleep();
  }
}

