# The simple control node for ROS and ArduCopter #

## Overview ##
This is simple control node which uses PD controller to hover above visual marker.
You can use this node with real quadrotor or for SITL mode. This node uses 'mavros' node to communicate with the drone.

## Install ##

Install the marker detector package:

```
#!bash

sudo apt-get install ros-indigo-ar-track-alvar
```

Clone this package into your ROS workspace:

```
#!bash

roscd
cd ../src #Navigate in your ROS user source files directory
git clone https://alexbuyval@bitbucket.org/alexbuyval/um_pixhawk.git
```
## Run the control node ##

Execute the following command in separate console window after you have run the SITL simulation or you have run the mavros node on the real drone:

```
#!bash

roslaunch um_pixhawk hovermarker.launch
```

Then you should arm the copter and set 'ALT_HOLD' mode by joystick. Then you can set a desired altitude by joystick.